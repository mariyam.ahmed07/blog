<?php

use App\Support\Date;
use App\Models\Building;

test('can list buildings', function () {
    $this->actingAs(createUser('apartments.manager'));
    Building::factory(5)->create();
    $response = $this->getJson('/api/buildings?page[size]=2&page[number]=2');
    $response->assertOk()->assertJson([
        'meta' => [
            'total' => 5,
            'per_page' => 2,
            'current_page' => 2,
        ]
    ])
    ->assertJsonStructure([
        'data' => [
            [
                'id',
                'name',
                'floors',
                'island' => [
                    'name',
                    'atoll' => [
                        'code',
                        'name'
                    ]
                ]
            ]
        ]
    ]);
    
})->group('buildings', 'buildings.listing');

test('can show building details', function () {
    $this->actingAs(createUser('apartments.manager'));
    $building = Building::factory()->create();
    $response = $this->getJson('/api/buildings/' . $building->id);
    $response->assertOk()->assertJson([
        'data' => [
            'id' => $building->id,
            'name' => $building->name,
            'floors' => $building->floors,
            'island' => [
                'name' => $building->island->name,
                'atoll' => [
                    'code' => $building->island->atoll->code,
                    'name' => $building->island->atoll->name,
                    'created_at' => Date::dateTimeFormat($building->island->atoll->created_at),
                    'updated_at' => Date::dateTimeFormat($building->island->atoll->updated_at),
                ],
                'created_at' => Date::dateTimeFormat($building->island->created_at),
                'updated_at' => Date::dateTimeFormat($building->island->updated_at),
            ],
            'created_at' => Date::dateTimeFormat($building->created_at),
            'updated_at' => Date::dateTimeFormat($building->updated_at),
        ]
    ]);
    
})->group('buildings', 'buildings.show');

test('can create a building', function () {
    $this->actingAs(createUser('apartments.manager'));
    $building = Building::factory()->make();
    $response = $this->postJson('/api/buildings', $building->toArray());
    $response->assertCreated()->assertJson([
        'data' => [
            'name' => $building->name,
            'floors' => $building->floors,
            'island' => [
                'name' => $building->island->name,
                'atoll' => [
                    'code' => $building->island->atoll->code,
                    'name' => $building->island->atoll->name,
                    'created_at' => Date::dateTimeFormat($building->island->atoll->created_at),
                    'updated_at' => Date::dateTimeFormat($building->island->atoll->updated_at),
                ],
                'created_at' => Date::dateTimeFormat($building->island->created_at),
                'updated_at' => Date::dateTimeFormat($building->island->updated_at),
            ]
        ]
    ]);
    
})->group('buildings', 'buildings.create');

test('can create a building throws error', function () {
    $this->actingAs(createUser('apartments.manager'));
    $response = $this->postJson('/api/buildings');
    $response->assertInvalid(['name', 'floors', 'island_id']);
    
})->group('buildings', 'buildings.create');

test('can update building details', function () {
    $this->actingAs(createUser('apartments.manager'));
    $building = Building::factory()->create();
    $new = Building::factory()->make();
    $response = $this->putJson('/api/buildings/' . $building->id, $new->toArray());
    $building->refresh();
    $response->assertOk()->assertJson([
        'data' => [
            'id' => $building->id,
            'name' => $new->name,
            'floors' => $new->floors,
            'island' => [
                'name' => $new->island->name,
                'atoll' => [
                    'code' => $new->island->atoll->code,
                    'name' => $new->island->atoll->name,
                    'created_at' => Date::dateTimeFormat($new->island->atoll->created_at),
                    'updated_at' => Date::dateTimeFormat($new->island->atoll->updated_at),
                ],
                'created_at' => Date::dateTimeFormat($new->island->created_at),
                'updated_at' => Date::dateTimeFormat($new->island->updated_at),
            ],
            'created_at' => Date::dateTimeFormat($building->created_at),
            'updated_at' => Date::dateTimeFormat($building->updated_at),
        ]
    ]);

    $this->assertDatabaseHas('buildings', [
        'id' => $building->id,
        'name' => $new->name,
        'floors' => $new->floors,
    ]);
    
})->group('buildings', 'buildings.update');

test('can delete a building', function () {
    $this->actingAs(createUser('apartments.manager'));
    $building = Building::factory()->create();
    $response = $this->deleteJson('/api/buildings/'.$building->id);
    $response->assertNoContent();

    $this->assertDatabaseMissing('buildings', ['id' => $building->id]);
    
})->group('buildings', 'buildings.delete');
