<?php

use App\Models\Apartment;
use App\Support\Date;

test('can list apartments', function () {
    $this->actingAs(createUser('apartments.manager'));
    Apartment::factory(5)->create();
    $response = $this->getJson('/api/apartments?page[size]=2&page[number]=2');
    $response->assertOk()->assertJson([
        'meta' => [
            'total' => 5,
            'per_page' => 2,
            'current_page' => 2,
        ]
    ])
    ->assertJsonStructure([
        'data' => [
            [
                'id',
                'number',
                'floor',
                'rooms',
                'area',
                'building' => [
                    'name',
                    'floors'
                ]
            ]
        ]
    ]);
    
})->group('apartments', 'apartments.listing');

test('can show apartment details', function () {
    $this->actingAs(createUser('apartments.manager'));
    $apartment = Apartment::factory()->create();
    $response = $this->getJson('/api/apartments/' . $apartment->id);
    $response->assertOk()->assertJson([
        'data' => [
            'id' => $apartment->id,
            'number' => $apartment->number,
            'floor' => $apartment->floor,
            'rooms' => $apartment->rooms,
            'area' => $apartment->area,
            'building' =>   [
                'id' => $apartment->building->id,
                'name' => $apartment->building->name,
                'floors' => $apartment->building->floors,
                'island' => [
                    'name' => $apartment->building->island->name,
                    'atoll' => [
                        'code' => $apartment->building->island->atoll->code,
                        'name' => $apartment->building->island->atoll->name,
                        'created_at' => Date::dateTimeFormat($apartment->building->island->atoll->created_at),
                        'updated_at' => Date::dateTimeFormat($apartment->building->island->atoll->updated_at),
                    ],
                    'created_at' => Date::dateTimeFormat($apartment->building->island->created_at),
                    'updated_at' => Date::dateTimeFormat($apartment->building->island->updated_at),
                ],
                'created_at' => Date::dateTimeFormat($apartment->building->created_at),
                'updated_at' => Date::dateTimeFormat($apartment->building->updated_at),
            ],
            'created_at' => Date::dateTimeFormat($apartment->building->created_at),
            'updated_at' => Date::dateTimeFormat($apartment->building->updated_at),
        ]
    ]);
    
})->group('apartments', 'apartments.show');

// test('can create a building', function () {
//     $this->actingAs(createUser('apartments.manager'));
//     $building = Building::factory()->make();
//     $response = $this->postJson('/api/apartments', $building->toArray());
//     $response->assertCreated()->assertJson([
//         'data' => [
//             'name' => $building->name,
//             'floors' => $building->floors,
//             'island' => [
//                 'name' => $building->island->name,
//                 'atoll' => [
//                     'code' => $building->island->atoll->code,
//                     'name' => $building->island->atoll->name,
//                     'created_at' => Date::dateTimeFormat($building->island->atoll->created_at),
//                     'updated_at' => Date::dateTimeFormat($building->island->atoll->updated_at),
//                 ],
//                 'created_at' => Date::dateTimeFormat($building->island->created_at),
//                 'updated_at' => Date::dateTimeFormat($building->island->updated_at),
//             ]
//         ]
//     ]);
    
// })->group('apartments', 'apartments.create');

// test('can create a building throws error', function () {
//     $this->actingAs(createUser('apartments.manager'));
//     $response = $this->postJson('/api/apartments');
//     $response->assertInvalid(['name', 'floors', 'island_id']);
    
// })->group('apartments', 'apartments.create');

// test('can update building details', function () {
//     $this->actingAs(createUser('apartments.manager'));
//     $building = Building::factory()->create();
//     $new = Building::factory()->make();
//     $response = $this->putJson('/api/apartments/' . $building->id, $new->toArray());
//     $building->refresh();
//     $response->assertOk()->assertJson([
//         'data' => [
//             'id' => $building->id,
//             'name' => $new->name,
//             'floors' => $new->floors,
//             'island' => [
//                 'name' => $new->island->name,
//                 'atoll' => [
//                     'code' => $new->island->atoll->code,
//                     'name' => $new->island->atoll->name,
//                     'created_at' => Date::dateTimeFormat($new->island->atoll->created_at),
//                     'updated_at' => Date::dateTimeFormat($new->island->atoll->updated_at),
//                 ],
//                 'created_at' => Date::dateTimeFormat($new->island->created_at),
//                 'updated_at' => Date::dateTimeFormat($new->island->updated_at),
//             ],
//             'created_at' => Date::dateTimeFormat($building->created_at),
//             'updated_at' => Date::dateTimeFormat($building->updated_at),
//         ]
//     ]);

//     $this->assertDatabaseHas('apartments', [
//         'id' => $building->id,
//         'name' => $new->name,
//         'floors' => $new->floors,
//     ]);
    
// })->group('apartments', 'apartments.update');

// test('can delete a building', function () {
//     $this->actingAs(createUser('apartments.manager'));
//     $building = Building::factory()->create();
//     $response = $this->deleteJson('/api/apartments/'.$building->id);
//     $response->assertNoContent();

//     $this->assertDatabaseMissing('apartments', ['id' => $building->id]);
    
// })->group('apartments', 'apartments.delete');
