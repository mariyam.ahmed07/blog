<?php

use App\Models\User;
use App\Support\Date;

test('can get auth user details', function () {
    $user = User::factory()->create();
    $this->actingAs($user);
    $response = $this->getJson('/api/user');
    $response->assertOk()->assertJson([
        'data' => [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'created_at' => Date::dateTimeFormat($user->created_at),
            'updated_at' => Date::dateTimeFormat($user->updated_at),
        ]
    ]);
    
})->group('users', 'users.auth');

test('can get auth user details with permissions', function () {
    $user = createUser('apartments.manager');
    $this->actingAs($user);
    $response = $this->getJson('/api/user?include=permissions,roles.permissions');
    $response->assertOk()->assertJson([
        'data' => [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'roles' => [
                [
                    'name' => 'apartments.manager',
                    'permissions' => [
                        ['name' => 'buildings.list'],
                        ['name' => 'buildings.show'],
                    ] 
                ]
            ],
            'permissions' => [],
            'created_at' => Date::dateTimeFormat($user->created_at),
            'updated_at' => Date::dateTimeFormat($user->updated_at),
        ]
    ]);
    
})->group('users', 'users.auth', 'users.auth.permissions');

test('can list users', function () {
    $this->actingAs(createUser('users.manager'));
    User::factory(4)->create();
    $response = $this->getJson('/api/users?page[size]=2&page[number]=2');
    $response->assertOk()->assertJson([
        'meta' => [
            'total' => 5, //including authenticated user
            'per_page' => 2,
            'current_page' => 2,
        ]
    ])
    ->assertJsonStructure([
        'data' => [
            [
                'id',
                'name',
                'email',
            ]
        ]
    ]);
    
})->group('users', 'users.listing');
