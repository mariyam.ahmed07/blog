<?php

namespace App\Support;

use DateTime;

class Date {

    const DEFAULT_DATETIME_FORMAT = 'Y-m-d H:i:s';
    const DEFAULT_DATE_FORMAT = 'Y-m-d';
    const DEFAULT_TIME_FORMAT = 'H:i:s';

    static function format(DateTime $datetime, $format) : string {
        return $datetime->format($format);
    }
    
    static function dateTimeFormat(DateTime $datetime = null) : ?string {
        if (is_null($datetime)) {
            return $datetime;
        }
        
        return $datetime->format(self::DEFAULT_DATETIME_FORMAT);
    }
    
    static function dateFormat(DateTime $datetime = null) : ?string {
        if (is_null($datetime)) {
            return $datetime;
        }
        
        return $datetime->format(self::DEFAULT_DATE_FORMAT);
    }

    static function timeFormat(DateTime $datetime = null) : ?string {
        if (is_null($datetime)) {
            return $datetime;
        }
        
        return $datetime->format(self::DEFAULT_TIME_FORMAT);
    }
}