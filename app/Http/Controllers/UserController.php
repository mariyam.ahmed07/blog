<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use Spatie\QueryBuilder\QueryBuilder;

class UserController extends Controller
{
    function authUser(Request $request) : UserResource {
        $user = $request->user();
        $user = QueryBuilder::for(User::class)
            ->findOrFail($user->id);

        return new UserResource($user);
    }
}
