<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreApartmentRequest;
use App\Http\Requests\UpdateApartmentRequest;
use App\Models\Apartment;

class AppController extends Controller
{
    public function index()
    {
        return view('dashboard');
    }
}
