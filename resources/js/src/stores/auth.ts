import Axios from '@/utils/Axios';
import { defineStore } from 'pinia';

export const useAuthStore = defineStore('auth', {
    state: () => ({
        authUser: { 
            name: null,
            email: null,
        },
    }),

    actions: {
        logout(callback) {
            Axios.post('/logout').then(() => {
                callback();
            });
        },

        async getUser() {
            try {
              const resp = await Axios.get('/api/user');
              this.authUser = resp.data.data;
            } catch (error) {
                alert('error: /api/user');
                //should handle error
            }
        }
    },

    getters: {
        user: (state) => {
            return state['authUser'];
        }
    },
});
